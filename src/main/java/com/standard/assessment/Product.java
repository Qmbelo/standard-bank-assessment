package com.standard.assessment;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "table_id_seq", sequenceName = "item_entity_id_seq", allocationSize = 1)
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "item_id", unique = true, nullable = false)
	private int id;

	private String name;

	private double cost;

	private int quantity;

//  Default constructor
	public Product() {
	}

	// Constructor
	public Product(int id, String name, double cost, int quantity) {
		this.id = id;
		this.name = name;
		this.cost = cost;
		this.quantity = quantity;
	}

	// Getters and Setters
	public double getCost() {
		return cost;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
